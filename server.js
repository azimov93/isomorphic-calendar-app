require('babel-core/register');
['.css', '.less', '.scss', '.styl', '.ttf', '.woff', '.woff2', '.png', '.svg'].forEach((ext) => require.extensions[ext] = () => {});
require('babel-polyfill');
require('server.js');