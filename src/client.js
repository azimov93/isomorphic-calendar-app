import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
// import rootReducer from './rootReducer';
import { Provider } from 'react-redux';
import { browserHistory, Router } from 'react-router';
import routes from './routes';
// import { persistData } from './middleware/middleware';
import configureStore from './store';

const composeEnhancers = compose;

// const store = createStore(
//     rootReducer,
//     composeEnhancers(applyMiddleware(thunk, persistData))
// );

const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory} routes={routes} />
    </Provider>,
  document.getElementById('app')
);
