import { applyMiddleware, combineReducers, createStore, compose } from 'redux';
import thunk from 'redux-thunk';
import { meetings } from './reducers/meetings';
import { form } from './reducers/form';
import { persistData } from './middleware/middleware';

const composeEnhancers = compose;

export default function() {
    const rootReducer = combineReducers({
        meetings: meetings,
        form: form
    });

    return createStore(rootReducer, composeEnhancers(applyMiddleware(thunk, persistData)));
}