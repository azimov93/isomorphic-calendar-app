global.Promise         = require('bluebird');

var webpack            = require('webpack');
var path               = require('path');
var ExtractTextPlugin  = require('extract-text-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');

var publicPath         = 'http://localhost:8050/public/assets';
var cssName            = process.env.NODE_ENV === 'production' ? 'styles-[hash].css' : 'styles.css';
var jsName             = process.env.NODE_ENV === 'production' ? 'bundle-[hash].js' : 'bundle.js';

var plugins = [
    new webpack.DefinePlugin({
        'process.env': {
            BROWSER:  JSON.stringify(true),
            NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development')
        }
    }),
    new ExtractTextPlugin(cssName),
];

if (process.env.NODE_ENV === 'production') {
    plugins.push(
        new CleanWebpackPlugin([ 'public/assets/' ], {
            root: __dirname,
            verbose: true,
            dry: false
        })
    );
    plugins.push(new webpack.optimize.DedupePlugin());
    plugins.push(new webpack.optimize.OccurenceOrderPlugin());
}

module.exports = {
    entry: ['babel-polyfill', './src/client.js'],
    debug: process.env.NODE_ENV !== 'production',
    resolve: {
        root:               path.join(__dirname, 'src'),
        modulesDirectories: ['node_modules'],
        extensions:         ['', '.js', '.jsx']
    },
    plugins,
    output: {
        path: `${__dirname}/public/assets/`,
        filename: jsName,
        publicPath
    },
    module: {
        loaders: [
            {
                test: /\.scss$/,
                loader: 'style!css?modules&localIdentName=[path][name]__[local]--[hash:base64:5]&importLoaders=1!postcss!sass'
            },
            { test: /\.(jpg|jpeg|gif|png)$/, loader: 'url-loader', query: {name: '/img/[name].[ext]'}},
            { test: /\.(woff|woff2|ttf|eot)/, loader: 'url-loader?limit=1024&name=fonts/[name].[ext]' },
            { test: /\.jsx?$/, loader: process.env.NODE_ENV !== 'production' ? 'babel!eslint-loader' : 'babel', exclude: [/node_modules/, /public/] },
            { test: /\.json$/, loader: 'json-loader' },
            { test: /\.svg$/, loader: 'file-loader', query: { name: '/img/[name].[ext]' }}
        ]
    },
    devtool: process.env.NODE_ENV !== 'production' ? 'source-map' : null,
    devServer: {
        headers: { 'Access-Control-Allow-Origin': '*' }
    }
};